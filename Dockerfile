FROM alpine:3.13
RUN apk add --no-cache libstdc++ procps  wget unzip
RUN cd / && \
    wget https://www.browserstack.com/browserstack-local/BrowserStackLocal-alpine.zip && \
    unzip BrowserStackLocal-alpine.zip && \
    rm BrowserStackLocal-alpine.zip && \
    chmod +x /BrowserStackLocal

ENTRYPOINT [ "/BrowserStackLocal"]
